// Fill out your copyright notice in the Description page of Project Settings.

#include "ShopBox.h"
#include "ShooterGameCharacter.h"



void AShopBox::BeginPlay()
{
	Super::BeginPlay();
	
}

void AShopBox::ReceiveActorBeginOverlap(AActor* OtherActor)
{
	AShooterGameCharacter* Player = Cast<AShooterGameCharacter>(OtherActor);
	if (!Player)
		return;
	Player->CanShop(true);
}

void AShopBox::ReceiveActorEndOverlap(AActor* OtherActor)
{
	AShooterGameCharacter* Player = Cast<AShooterGameCharacter>(OtherActor);
	if (!Player)
		return;
	Player->CanShop(false);
}
