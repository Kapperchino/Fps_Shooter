// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ShooterGameMode.h"
#include "LobbyGameMode.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API ALobbyGameMode : public AShooterGameMode
{
	GENERATED_BODY()
	
	virtual void PostLogin(APlayerController* NewPlayer) override;
	virtual void Logout(AController* Exiting) override;

	void JoinLobby();

	int32 PlayerCount = 0;
	int32 MaxPlayers = 0;
	class UShooterGameInstance* GameInstance = nullptr;
	
	
};
