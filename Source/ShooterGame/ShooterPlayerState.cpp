// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterPlayerState.h"
#include "ShooterGameCharacter.h"
#include "ShooterPlayerController.h"
#include "UnrealNetwork.h"
#include "ShooterGameState.h"

AShooterPlayerState::AShooterPlayerState()
{
	TeamNumber = 0;
	NumKills = 0;
	NumDeaths = 0;
	bQuitter = false;
}

void AShooterPlayerState::Reset()
{
	Super::Reset();

	NumKills = 0;
	NumDeaths = 0;
	bQuitter = false;
}

void AShooterPlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AShooterPlayerState, TeamNumber);
	DOREPLIFETIME(AShooterPlayerState, NumKills);
	DOREPLIFETIME(AShooterPlayerState, NumDeaths);
}

void AShooterPlayerState::UnregisterPlayerWithSession()
{
	if (!bFromPreviousLevel)
	{
		Super::UnregisterPlayerWithSession();
	}
}

void AShooterPlayerState::SetTeamNum(int32 NewTeamNum)
{
	TeamNumber = NewTeamNum;
	UpdateTeamColors();
}

void AShooterPlayerState::ClientInitialize(AController* InController)
{
	Super::ClientInitialize(InController);

	UpdateTeamColors();
}

void AShooterPlayerState::OnRep_TeamColor()
{
	UpdateTeamColors();
}

void AShooterPlayerState::SetQuitter(bool bInQuitter)
{
	bQuitter = bInQuitter;
}

void AShooterPlayerState::CopyProperties(APlayerState* PlayerState)
{
	Super::CopyProperties(PlayerState);

	AShooterPlayerState* ShooterPlayer = Cast<AShooterPlayerState>(PlayerState);
	if (ShooterPlayer)
	{
		ShooterPlayer->TeamNumber = TeamNumber;
	}
}

void AShooterPlayerState::UpdateTeamColors()
{
	AShooterGameCharacter* Player = Cast<AShooterGameCharacter>(GetOwner());
	if (Player&&Player->GetController())
	{
		//UpdateTeamColors
	}

}

void AShooterPlayerState::ScoreKill(AShooterPlayerState* Victim, int32 Points)
{
	NumKills++;
	ScorePoints(Points);
}

void AShooterPlayerState::ScoreDeath(AShooterPlayerState* KilledBy, int32 Points)
{
	NumDeaths++;
	ScorePoints(Points);
}

FString AShooterPlayerState::GetShortPlayerName() const
{
	if (PlayerName.Len() > 16)
	{
		return PlayerName.Left(16) + "...";
	}
	return PlayerName;
}

void AShooterPlayerState::ScorePoints(int32 Points)
{
    AShooterGameState* const MyGameState = GetWorld()->GetGameState<AShooterGameState>();
	if (MyGameState&&TeamNumber >= 0)
	{
		if (TeamNumber >= MyGameState->TeamScores.Num())
		{
			MyGameState->TeamScores.AddZeroed(TeamNumber - MyGameState->TeamScores.Num() + 1);
		}
		MyGameState->TeamScores[TeamNumber] += Points;
	}
	Score += Points;
}

void AShooterPlayerState::InformAboutKill_Implementation(class AShooterPlayerState* KillerPlayerState, const UDamageType* KillerDamageType, class AShooterPlayerState* KilledPlayerState)
{
	if (!KilledPlayerState->UniqueId.IsValid())return;
	
	for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
	{
		AShooterPlayerController* TestPC = Cast<AShooterPlayerController>(*It);
		if (TestPC&&TestPC->IsLocalController())
		{
			ULocalPlayer* LocalPlayer = Cast<ULocalPlayer>(TestPC->Player);
			TSharedPtr<const FUniqueNetId> LocalID = LocalPlayer->GetCachedUniqueNetId();
			
			if (LocalID.IsValid() && *LocalPlayer->GetCachedUniqueNetId() == *KilledPlayerState->UniqueId)
			{
				TestPC->OnKill();
			}
		}
	}
}

void AShooterPlayerState::BroadcastDeath_Implementation(class AShooterPlayerState* KillerPlayerState, const UDamageType* KillerDamageType, class AShooterPlayerState* KilledPlayerState)
{
	for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
	{
		// all local players get death messages so they can update their huds.
		AShooterPlayerController* TestPC = Cast<AShooterPlayerController>(*It);
		if (TestPC && TestPC->IsLocalController())
		{
			//todo add broadcasting
		}
	}
}
