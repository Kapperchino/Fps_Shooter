// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "ShopBox.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FShopDelegate);

UCLASS()
class SHOOTERGAME_API AShopBox : public ATriggerBox
{
	GENERATED_BODY()

	virtual void BeginPlay() override;
	
	UFUNCTION()
		void ReceiveActorBeginOverlap(AActor* OtherActor);

	UFUNCTION()
		void ReceiveActorEndOverlap(AActor* OtherActor);
	
};
