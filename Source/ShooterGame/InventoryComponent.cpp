// Fill out your copyright notice in the Description page of Project Settings.

#include "InventoryComponent.h"
#include "PickUp.h"
#include "Gun.h"
#include "ShooterGameCharacter.h"
#include "Components/BoxComponent.h"
#include "Engine/Texture2D.h"
#include "UnrealNetwork.h"


// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	SetIsReplicated(true);
	Inventory.Init(nullptr, 7);
	InventoryIndex = Inventory.Find(nullptr);

	// ...
}

void UInventoryComponent::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(UInventoryComponent, Inventory, COND_OwnerOnly);
	DOREPLIFETIME(UInventoryComponent, CurrentPickUp);
}

int32 UInventoryComponent::AmountOfNull()
{
	int count = 0;
	for (int32 x = 0; x < Inventory.Num(); x++)
	{
		if (Inventory[x] == nullptr)
		{
			count++;
		}
	}
	return count;
}

void UInventoryComponent::ClearInventory()
{
	for (int32 x = 0; x < InventorySize; x++)
	{
		if (Inventory[x])
		{
			Inventory[x]->OnThow();
		}
	}
}

bool UInventoryComponent::SwapUp()
{
	if (!ensure(Player))return false;
	APickUp* TempPickUp = GetSwapUp();
	if (TempPickUp)
	{
		if (CurrentPickUp)
		{
			if (!Player)
			{
				Player = Cast<AShooterGameCharacter>(GetOwner());
			}
			CurrentPickUp->OnSwap();
		}

		CurrentPickUp = TempPickUp;
		CurrentPickUp->OnPickedUp(Player);
		return true;
	}
	
	return false;
}

bool UInventoryComponent::SwapDown()
{
	if (!ensure(Player))return false;
	APickUp* TempPickUp = GetSwapDown();
	if (TempPickUp)
	{
		if (CurrentPickUp)
		{
			CurrentPickUp->OnSwap();
		}
		CurrentPickUp = TempPickUp;
		CurrentPickUp->OnPickedUp(Player);

		return true;
	}
	
	return false;
}


APickUp* UInventoryComponent::GetSwapUp()
{
	for (int32 x = InventoryIndex-1; x >= 0; x--)
	{
		if (Inventory[x] && x != InventoryIndex)
		{
			InventoryIndex = x;
			return Inventory[x];
		}
	}

	for (int32 x = Inventory.Num()-1; x > InventoryIndex; x--)
	{
		if (Inventory[x] && x != InventoryIndex)
		{
			InventoryIndex = x;
			return Inventory[x];
		}
	}
	return nullptr;
}

APickUp* UInventoryComponent::GetSwapDown()
{
	
	for (int32 x = InventoryIndex + 1; x < Inventory.Num(); x++)
	{
		if (Inventory[x]&&x!=InventoryIndex)
		{
				InventoryIndex = x;
				return Inventory[x];
		}
	}
	

	for (int32 x = 0; x < InventoryIndex; x++)
	{
		if (Inventory[x] && x != InventoryIndex)
		{
			InventoryIndex = x;
			return Inventory[x];
		}
	}
	return nullptr;
}

UTexture2D* UInventoryComponent::GetThumbnailAtSlot(int32 slot)
{
	if (!Inventory[slot])
		return nullptr;

	return Inventory[slot]->ThumbNail;
}

APickUp* UInventoryComponent::GetCurrentPickup()
{
	return CurrentPickUp;
}


void UInventoryComponent::SetInvntoryIndexToValue(int32 Index, APickUp* Other)
{
	if ((Index<0 || Index>Inventory.Num()))
		return;

	Inventory[Index] = Other;
}

void UInventoryComponent::PickUp(APickUp* OtherPickup, bool bPickUpByHand)
{
	if (!ensure(Player))return;
	if (CurrentPickUp)
	{
		CurrentPickUp->OnSwap();
	}
	AddItemToInventory(OtherPickup, bPickUpByHand);
	CurrentPickUp = Inventory[InventoryIndex];
	CurrentPickUp->OnPickedUp(Player);

}

void UInventoryComponent::Server_ThrowPickUp_Implementation()
{
	ThrowPickUp();
}

bool UInventoryComponent::Server_ThrowPickUp_Validate()
{
	return true;
}

void UInventoryComponent::ThrowPickUp()
{
	if (Player->Role < ROLE_Authority)
	{
		Server_ThrowPickUp();
	}
	ThrowCurrentPickup();
}

void UInventoryComponent::ClearCurrentPickUp()
{
	if (!Player)return;
	CurrentPickUp = nullptr;
	Inventory[InventoryIndex] = nullptr;
	SwapDown();
}

void UInventoryComponent::ThrowCurrentPickup()
{
	if (!Player)return;
	CurrentPickUp->OnThow();
	CurrentPickUp = nullptr;
	Inventory[InventoryIndex] = nullptr;
	SwapDown();
}



void UInventoryComponent::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (Player->HasAuthority())
	{
		APickUp* Pickup = Cast<APickUp>(OtherActor);
		if (!Pickup||!Pickup->bCanBePickedUp)return;

		EquipPickUp(Pickup, false);
	}
}

bool UInventoryComponent::AddItemToInventory(APickUp*& Item, bool bPickUpByHand)
{
	if (!Item)
		return false;
	if (!ensure(Player))return false;

	const int32 Addplace = GetSpotToAdd(Item);

	if (Addplace == -1)
	{
		UE_LOG(LogTemp, Warning, TEXT("inventory full"));
		return false;
	}
	if (Inventory[Addplace]&&bPickUpByHand)
	{
		Inventory[Addplace]->OnThow();
		Inventory[Addplace] = nullptr;
		Inventory[Addplace] = Item;
		InventoryIndex = Addplace;
		return true;
	}
	if (!Inventory[Addplace])
	{
		Inventory[Addplace] = Item;
		InventoryIndex = Addplace;
		return true;
	}
	return false;
	

}




int32 UInventoryComponent::GetInventoryIndex()
{
	return InventoryIndex;
}

int32 UInventoryComponent::GetSpotToAdd(APickUp* Item)
{
	EItemType Type = Item->ItemType;
	if (Type == EItemType::EG_Rifle)
	{
		return 0;
	}
	else if (Type == EItemType::EG_Pistol)
	{
		return 1;
	}
	else if (Type == EItemType::EG_Knife)
	{
		return 2;
	}
	else if(Type == EItemType::EG_Gernade)
	{
		for (int x = 3; x < Inventory.Num(); x++)
		{
			if (!Inventory[x])
				return x;
		}
	}

	
	return -1;
	
}


void UInventoryComponent::EquipPickUp(APickUp* NewPickUp, bool bPickUpByHand)
{
	if (NewPickUp)
	{
		if (Player->Role<ROLE_Authority)
		{
			Server_EquipPickup(NewPickUp, bPickUpByHand);
		}
		
		PickUp(NewPickUp, bPickUpByHand);
		
	}
}

void UInventoryComponent::Server_EquipPickup_Implementation(APickUp* NewItem, bool bPickupByHand)
{
	EquipPickUp(NewItem, bPickupByHand);
}



// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	Player = Cast<AShooterGameCharacter>(GetOwner());
	Player->GetPickupBox()->OnComponentBeginOverlap.AddDynamic(this, &UInventoryComponent::OnOverlapBegin);
	
	
}


// Called every frame
void UInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool UInventoryComponent::Server_EquipPickup_Validate(APickUp* NewItem, bool bPickupByHand)
{
	return true;
}
