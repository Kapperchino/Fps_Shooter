// Fill out your copyright notice in the Description page of Project Settings.

#include "PickUp.h"
#include "Components/BoxComponent.h"
#include "ShooterGameCharacter.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "UnrealNetwork.h"


// Sets default values
APickUp::APickUp()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	bReplicateMovement = true;

	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FirstPersonMesh"));
	Mesh1P->MeshComponentUpdateFlag = EMeshComponentUpdateFlag::OnlyTickPoseWhenRendered;
	Mesh1P->SetVisibility(true);
	Mesh3P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("ThirdPersonMesh"));
	Mesh3P->MeshComponentUpdateFlag = EMeshComponentUpdateFlag::OnlyTickPoseWhenRendered;
	Mesh3P->SetOwnerNoSee(true);
	Mesh3P->SetVisibility(false);
	SetRootComponent(Mesh1P);
	Mesh3P->SetupAttachment(Mesh1P);
	Mesh3P->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Mesh3P->SetSimulatePhysics(false);
	Speed = 3000;

	Price = 0;

}

void APickUp::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APickUp, MyPawn);


}
// Called when the game starts or when spawned
void APickUp::BeginPlay()
{
	Super::BeginPlay();
	
	
}


void APickUp::Use()
{

}

FString APickUp::GetName()
{
	return Name;
}

void APickUp::OnPickup(const APickUp* LastWeapon)
{
	AttachMesh();
	bPendingEquip = true;

	//TODO: Play Animation

}

void APickUp::OnPickedUp(class AShooterGameCharacter* NewOwner)
{
	Mesh1P->SetVisibility(true);
	Mesh1P->bOnlyOwnerSee = true;
	Mesh1P->SetCastShadow(false);
	Mesh1P->SetSimulatePhysics(false);
	Mesh1P->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Mesh3P->SetVisibility(true);
	Mesh3P->bOwnerNoSee = true;
	SetOwningPawn(NewOwner);
	bIsEquipped = true;
	AttachMesh();
	Mesh1P->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
}


void APickUp::AttachMesh()
{
	if (!ensure(MyPawn))
		return;

	USkeletalMeshComponent* PawnMesh1P = nullptr;
	USkeletalMeshComponent* PawnMesh3P = nullptr;
	MyPawn->GetPawnMesh(PawnMesh1P, PawnMesh3P);
	if (!PawnMesh1P) {
		UE_LOG(LogTemp, Warning, TEXT("lol"));
	}
	Mesh1P->AttachToComponent(PawnMesh1P, FAttachmentTransformRules::SnapToTargetIncludingScale, "GripPoint");
	Mesh3P->AttachToComponent(PawnMesh3P, FAttachmentTransformRules::SnapToTargetIncludingScale, "GripPoint");
}



void APickUp::DetachMesh()
{
	Mesh1P->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
	Mesh3P->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
}


float APickUp::GetSpeed()
{
	return Speed;
}

void APickUp::SetOwningPawn(class AShooterGameCharacter* Owner)
{
	if (MyPawn != Owner)
	{
		MyPawn = Owner;
		SetOwner(Owner);
	}
}


bool APickUp::CanThrow()
{
	return bThrowable;
}

void APickUp::OnThow()
{
	if (!MyPawn)return;
	Mesh1P->SetVisibility(true);
	Mesh1P->SetCastShadow(true);
	Mesh1P->SetSimulatePhysics(true);
	Mesh1P->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	Mesh1P->bOnlyOwnerSee = false;
	Mesh3P->SetCastShadow(false);
	Mesh3P->SetVisibility(false);

	FVector Direction = GetThrowDirection();
	Mesh1P->AddImpulse(Direction);

	SetOwningPawn(nullptr);
	DetachMesh();
	


	GetWorld()->GetTimerManager().SetTimer(ThrowTimer, this, &APickUp::OnAfterThrow,1, false,0.8);
	bIsEquipped = false;
	bPendingEquip = false;


	
}


USkeletalMeshComponent* APickUp::GetMesh()
{
	if (MyPawn&&MyPawn->IsLocallyControlled())
	{
		return Mesh1P;
	}
	return Mesh3P;
}


void APickUp::BeginUse()
{

}


void APickUp::EndUse()
{

}

void APickUp::OnSwap()
{
	if (!MyPawn)
		return;
	Mesh1P->SetVisibility(false);
	Mesh1P->SetSimulatePhysics(false);
	USkeletalMeshComponent* PawnMesh1P = nullptr;
	USkeletalMeshComponent* PawnMesh3P = nullptr;
	MyPawn->GetPawnMesh(PawnMesh1P, PawnMesh3P);
	if (ItemType == EItemType::EG_Rifle)
	{
		Mesh3P->AttachToComponent(PawnMesh3P, FAttachmentTransformRules::SnapToTargetIncludingScale, "Rifle_ThirdPerson");
	}
	else if (ItemType == EItemType::EG_Pistol)
	{
		Mesh3P->AttachToComponent(PawnMesh3P, FAttachmentTransformRules::SnapToTargetIncludingScale, "Pistol_ThirdPerson");
	}
	else if (ItemType == EItemType::EG_Knife)
	{
	}
	else
	{
		Mesh3P->AttachToComponent(PawnMesh3P, FAttachmentTransformRules::SnapToTargetIncludingScale, "Gernade1");
	}


}


void APickUp::OnAfterThrow()
{
	if (HasAuthority())
	{
		Mesh1P->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
		GetWorld()->GetTimerManager().ClearTimer(ThrowTimer);
	}
}

void APickUp::OnRep_MyPawn()
{
	if (MyPawn)
	{
		SetOwningPawn(MyPawn);
	}
	else
	{
		if (Role == ROLE_Authority)
		{
			SetOwningPawn(nullptr);
		}
		if (IsAttachedToPawn())
		{
			OnThow();
		}
	}
}

FVector APickUp::GetThrowDirection()
{
	if (!MyPawn)return FVector::ZeroVector;
	FRotator PlayerRotation;
	FVector PlayerLocation;
	MyPawn->UpdateRay(0, PlayerLocation, PlayerRotation);
	FRotator AdustRotator(45, 0, 0);
	FRotator AdustedPlayerRotation = PlayerRotation + AdustRotator;
	if (AdustedPlayerRotation.Pitch > 95)
		AdustedPlayerRotation.Pitch = 95;
	FVector AdustedRotation = AdustedPlayerRotation.Vector().GetSafeNormal();
	FVector Direction = AdustedRotation * Speed + MyPawn->GetVelocity() * 3;
	return Direction;
}




