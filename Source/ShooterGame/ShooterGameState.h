// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "ShooterPlayerState.h"
#include "ShooterGameState.generated.h"


/**
 * 
 */
typedef TMap<int32, TWeakObjectPtr<AShooterPlayerState> > RankedPlayerMap;

UCLASS()
class SHOOTERGAME_API AShooterGameState : public AGameState
{
	GENERATED_BODY()
	
public:

	UPROPERTY(Transient, Replicated)
		int32 NumTeams;

	UPROPERTY(Transient, Replicated)
		TArray<int32> TeamScores;

	UPROPERTY(Transient, Replicated)
		int32 RemainingTime;

	UPROPERTY(Transient, Replicated)
		bool bTimerPaused;
	
	void GetRankedMap(int32 TeamIndex, RankedPlayerMap& OutRankedMap) const;

	void RequestFinishAndExitToMainMenu();
};
