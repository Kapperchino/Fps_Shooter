// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ZoomComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SHOOTERGAME_API UZoomComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UZoomComponent();

	void ZoomIn(bool bHold);
	void ZoomOut(bool bHold);


protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	bool bZoomingIn = false;
	bool bIsZoomedIn = false;
	
	//max zoom min fov
	float GetMaxZoom();

	float GetFov();

	
	class UCameraComponent* CameraComponent = nullptr;
	

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
	
};
