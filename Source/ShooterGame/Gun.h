// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickUp.h"
#include "Gun.generated.h"

/**
*
*/
UENUM(BlueprintType)
enum class EWeaponState : uint8
{
	Idle,
	Firing,
	Reloading,
	Equipping,
};

USTRUCT()
struct FWeaponEffects
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly)
	UParticleSystem* MuzzleParticle;

	UPROPERTY(EditDefaultsOnly)
	FName MuzzleSocketName;

	UPROPERTY(EditDefaultsOnly)
	UParticleSystem* ImpactBloodParticle;

	UPROPERTY(EditAnywhere, Category = Gun)
	class USoundBase* FireSound;

	UPROPERTY(EditAnywhere)
	UMaterialInterface* BulletDecal = nullptr;

	UPROPERTY(EditAnywhere, Category = Gun)
	class UAnimMontage* FireAnimation;

	UPROPERTY(EditAnywhere, Category = Gun)
	class UAnimMontage* ReloadAnimation;
};



USTRUCT()
struct FWeaponData
{
	GENERATED_BODY()


	UPROPERTY(EditDefaultsOnly)
	int32 Damage;

	UPROPERTY(EditDefaultsOnly)
	bool bAuto = true;

	UPROPERTY(EditDefaultsOnly)
	class UCurveVector* Recoil = nullptr;

	UPROPERTY(EditDefaultsOnly)
	class UCurveVector* Spread = nullptr;

	UPROPERTY(EditDefaultsOnly)
	float Range;

	UPROPERTY(EditDefaultsOnly)
	float FireRate;

	UPROPERTY(EditDefaultsOnly)
	int32 ClipAmmo;

	UPROPERTY(EditDefaultsOnly)
	int32 TotalAmmo;

	FWeaponData()
	{
		Damage = 20;
		bAuto = true;
		Range = 1000;
		FireRate = 0.1;
		ClipAmmo = 30;
		TotalAmmo = 120;
	}
};



UCLASS()
class SHOOTERGAME_API AGun : public APickUp
{
	GENERATED_BODY()

public:

	AGun();

	UFUNCTION(BlueprintCallable)
		FORCEINLINE int32 GetCurrentAmmo() { return CurrentClipAmmo; }

	UFUNCTION(BlueprintCallable)
		FORCEINLINE int32 GetCurrentTotalAmmo() { return CurrentTotalAmmo; }

	UFUNCTION(BlueprintCallable)
		void Reload();


	void AddAmmo(int32 Amount);


	UFUNCTION(BlueprintCallable)
		bool CanFire();

	UFUNCTION(NetMulticast,Reliable)
		void MultiCastPlayGunEffects();

	float CalculatePenetrationLength(FHitResult& EntryHit, FHitResult& ExitHit);

	float CalculatePenetrationDamage(FHitResult& EntryHit, float Damage, float Distance);

	void HandleRecoil();

	void SimulateFire();
	void OnFire();

	virtual void BeginUse() override;

	virtual void EndUse() override;


	void OnReFire();

	void CreateBulletHole(FHitResult& Object);

	FWeaponEffects GetWeaponEffects() { return WeaponEffects; }

	FWeaponData GetWeaponData() { return WeaponData; }

	bool bDebugHit = false;

protected:

	EWeaponState WeaponState = EWeaponState::Idle;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	FWeaponEffects WeaponEffects;
	

	UPROPERTY(EditDefaultsOnly)
	FWeaponData WeaponData;

	UPROPERTY(Transient,Replicated)
	int32 CurrentClipAmmo;

	UPROPERTY(Transient,Replicated)
	int32 CurrentTotalAmmo;

	UPROPERTY(EditDefaultsOnly)
	bool bFire = true;

	UPROPERTY(Transient, Replicated)
	bool bPendingReload = false;

	UPROPERTY(Replicated)
	int32 BulletIndex = 0;

	int32 Damage = 0;

	bool bWantsToFire = false;

	FTimerHandle GunTimer;

	bool TraceHit(TArray<FHitResult> &OutHitResult, TArray<FHitResult> &OutReverseHit);
	
	
	void ProcessDamage(const TArray<FHitResult>& HitResultList, const TArray<int32>& DamageList);

	void CalculateDamageDone(const FHitResult Hit, const int32 Damage);
	/** AnimMontage to play each time we fire */
	

	

};
