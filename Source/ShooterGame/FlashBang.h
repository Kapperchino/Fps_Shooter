// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Throwable.h"
#include "FlashBang.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API AFlashBang : public AThrowable
{
	GENERATED_BODY()
	
public:

	virtual void Explode() override;

protected:
	
	UPROPERTY(EditDefaultsOnly)
		float FlashMaxRange = 5000;

	float CalculateFlashAngle(AShooterGameCharacter* const Character, const FHitResult& FlashBangLocation) const;
	
};
