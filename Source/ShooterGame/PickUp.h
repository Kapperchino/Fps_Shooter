// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/DataTable.h"
#include "PickUp.generated.h"


UENUM(BlueprintType)
enum class EItemType : uint8
{
	EG_Rifle,
	EG_Knife,
	EG_Pistol,
	EG_Gernade,
};


UCLASS()
class SHOOTERGAME_API APickUp : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	APickUp();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	bool bThrowable = true;

public:
	// Called every frame

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Gun)
		EItemType ItemType = EItemType::EG_Rifle;


	UPROPERTY(EditAnywhere, Category = Gun)
		UTexture2D* ThumbNail;

	UFUNCTION(BlueprintCallable)
		void Use();

	UFUNCTION(BlueprintCallable)
		FString GetName();

	UFUNCTION(BlueprintCallable)
		virtual void OnThow();

	USkeletalMeshComponent* GetMesh();


	virtual void BeginUse();

	virtual void EndUse();

	void OnPickup(const APickUp* LastWeapon);

	void OnPickedUp(class AShooterGameCharacter* NewOwner);

	void AttachMesh();

	void DetachMesh();

	float GetSpeed();

	void SetOwningPawn(class AShooterGameCharacter* Owner);

	bool CanThrow();

	void OnSwap();

	void OnAfterThrow();
	bool IsEquipped() { return bIsEquipped; }

	bool IsAttachedToPawn() { return bIsEquipped || bPendingEquip; }

	//can be picked up by overlapping with volume
	UPROPERTY(Replicated)
	bool bCanBePickedUp= true;
protected:

	UFUNCTION()
		void OnRep_MyPawn();

	FTimerHandle ThrowTimer;

	
	FVector GetThrowDirection();

	UPROPERTY(VisibleDefaultsOnly)
		USkeletalMeshComponent* Mesh1P = nullptr;
	
	UPROPERTY(VisibleDefaultsOnly)
		USkeletalMeshComponent* Mesh3P = nullptr;


	UPROPERTY(VisibleDefaultsOnly)
		class USceneComponent* root = nullptr;


	UPROPERTY(EditAnywhere, Category = Gun)
		FString Name;

	UPROPERTY(EditAnywhere, Category = Gun)
		int32 Price;

	UPROPERTY(EditDefaultsOnly, Category = Gun)
		float Speed;

	UPROPERTY(Transient,ReplicatedUsing = OnRep_MyPawn)
	class AShooterGameCharacter* MyPawn = nullptr;


	bool bIsEquipped = false;

	bool bPendingEquip = false;



};
