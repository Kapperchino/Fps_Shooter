// Fill out your copyright notice in the Description page of Project Settings.

#include "Throwable.h"
#include "Kismet/GameplayStatics.h"
#include "PhysicsEngine/RadialForceComponent.h"
#include "InventoryComponent.h"
#include "ShooterGameCharacter.h"
#include "GameFramework/ProjectileMovementComponent.h"




AThrowable::AThrowable()
{
	bThrowable = true;
	Explosion = CreateDefaultSubobject<URadialForceComponent>("Explosion Force");
}

void AThrowable::BeginUse()
{
	//Play the hold animation
}

void AThrowable::EndUse()
{
	if (!MyPawn)return;
	GetWorld()->GetTimerManager().SetTimer(ThrowableTimer, this, &AThrowable::Explode, Delay, false, Delay);
	Mesh1P->SetVisibility(true);
	Mesh1P->SetCastShadow(true);
	Mesh1P->bOnlyOwnerSee = false;
	Mesh1P->SetSimulatePhysics(true);
	Mesh1P->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	FVector Direction = GetThrowDirection();
	Mesh1P->AddImpulse(Direction);
	UInventoryComponent* Component = MyPawn->FindComponentByClass<UInventoryComponent>();
	if (Component)
	{
		Component->ThrowCurrentPickup();
	}
	
	SetOwningPawn(nullptr);
	DetachMesh();
}

void AThrowable::Explode()
{
	Explosion->FireImpulse();
	
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(), MaxDamage, MinDamge, GetActorLocation(), 100, 500, 50, UDamageType::StaticClass()
		, TArray<AActor*>());
	
}

void AThrowable::OnThow()
{
	
}

float AThrowable::AngleBetweenTwoVectors(const FVector A, const FVector B)
{
	FVector Vector1 = A.GetSafeNormal();
	FVector Vector2 = B.GetSafeNormal();
	float DotResult = A|B;
	return FMath::Acos(DotResult);
}
