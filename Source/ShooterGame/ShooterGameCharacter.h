// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ShooterGameCharacter.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FShooterDelegate);
class UInputComponent;
class APickUp;
class AGun;

UENUM(BlueprintType)
enum class ECharState : uint8
{
	ES_UnArmed,
	ES_Knife,
	ES_Pistol,
	ES_AK47,
};

UCLASS(config = Game)
class AShooterGameCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USkeletalMeshComponent* Mesh1P;

	/** Gun mesh: 1st person view (seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		class USkeletalMeshComponent* FP_Gun;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FirstPersonCameraComponent;

	class UZoomComponent* ZoomComponent = nullptr;



public:
	AShooterGameCharacter();


	UPROPERTY(BlueprintAssignable)
		FShooterDelegate OnSwap;

	UPROPERTY(BlueprintAssignable)
		FShooterDelegate OnDeath;

	UPROPERTY(BlueprintAssignable)
	FShooterDelegate ShotEnd;

	UPROPERTY(BlueprintAssignable)
	FShooterDelegate ShotStart;

	UPROPERTY(BlueprintAssignable)
		FShooterDelegate OnShot;

	UPROPERTY(BlueprintReadWrite,Replicated)
		float AimPitch;



	void SwapUp();
	void SwapDown();

	

	UPROPERTY(EditDefaultsOnly, ReplicatedUsing = OnRep_Health)
		int32 CurrentHealth;

	const int32 MaxHealth = 100;

protected:
	virtual void BeginPlay();

	virtual void Tick(float DeltaTime) override;


public:
	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;

	FVector UpdateRay(float Range, FVector& OutPlayerLocation, FRotator& OutPlayerRotation);

	/** Sound to play each time we fire */

	void ZoomIn();
	void ZoomOut();
	
	

	UFUNCTION(BlueprintCallable)
		void ChangeMoney(int32 Amount);

	UFUNCTION(BlueprintCallable)
		void AccessShop();

	void BeginFire();

	void EndFire();

	UFUNCTION(Server,Reliable,WithValidation)
	void ServerBeginFire();

	UFUNCTION(Server,Reliable,WithValidation)
	void ServerEndFire();

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_Reload();


	UFUNCTION(BlueprintCallable)
		bool IsDead();

	void CanShop(bool Condition);
	
	void StartCrouch();

	void EndCrouch();

	class UBoxComponent* GetPickupBox() { return PickupBox; }



	

	UFUNCTION(BlueprintCallable)
		void GetPawnMesh(USkeletalMeshComponent*& FirstPerson, USkeletalMeshComponent*& ThirdPerson);

	void CalculatePawnRotation(float Deltatime);
	UFUNCTION(BlueprintCallable)
	FRotator GetAimOffsets();

	void Death(float Damage, struct FDamageEvent const & DamageEvent, class AController* EventInstigator, AActor* DamageCauser);

	UFUNCTION(NetMulticast, Unreliable, WithValidation)
	void PlayDeathAnimation();


	void SetRagdollPhysics();
	/** Whether to use motion controller location for aiming. */

protected:

	



	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles stafing movement, left and right */
	void MoveRight(float Val);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	


	//TArray<FHitResult> HitResult;

	void ShowInventory();

	UFUNCTION()
		void ThrowPickUp();

	UFUNCTION(BlueprintCallable)
		void Reload();

		void EquipPickUp();
	UFUNCTION()
		virtual void OnRep_Health();



protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	// End of APawn interface

	UPROPERTY(VisibleDefaultsOnly)
	class UInventoryComponent* InventoryComp = nullptr;

	UPROPERTY(VisibleDefaultsOnly)
	class UBoxComponent* PickupBox = nullptr;

	UPROPERTY(EditDefaultsOnly)
	UAnimMontage* DeathAnim;


	float PickUpReach;

	//Line Trace Variables
	//Collision params
	FCollisionQueryParams TraceParams;
	FCollisionResponseParams* ResponseParams;

	bool Shop = false;

	

	UPROPERTY(BlueprintReadOnly)
	FRotator StartFireRotation;

	UPROPERTY(Replicated)
	bool bIsDead = false;

	bool RecoilStart = true;

	bool bIsPaused = false;

	bool bIsDying = false;


	UPROPERTY(EditDefaultsOnly,Category = Gun)
	float FireRate = 0.10;

	UPROPERTY(EditDefaultsOnly)
		int32 Money;

	bool bWantsToFire = false;


	void CheckForPickUps();

	void ResetAim(float DeltaTime);

	void PauseGame();

	void OnReFire();
	





public:
	/** Returns Mesh1P subobject **/
	FORCEINLINE class USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }
	/** Returns FirstPersonCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }

	virtual float TakeDamage(float Damage, struct FDamageEvent const & DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

private:

	





	int32 Damage;

};


