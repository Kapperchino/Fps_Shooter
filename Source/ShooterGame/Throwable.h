// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickUp.h"
#include "Throwable.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API AThrowable : public APickUp
{
	GENERATED_BODY()
public:

	AThrowable();

	
	virtual void BeginUse() override;
	virtual void EndUse() override;

	virtual void Explode();

	virtual void OnThow()override;

	float AngleBetweenTwoVectors(const FVector A, const FVector B);

protected:

	UPROPERTY(EditDefaultsOnly)
		float Delay = 3;

	float MaxDamage = 80;

	float MinDamge = 10;

	UPROPERTY(VisibleAnywhere)
	class URadialForceComponent* Explosion = nullptr;
	FTimerHandle ThrowableTimer;
	
};
