// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "ShooterGameMode.generated.h"

/**
 * 
 */

class AShooterPlayerState;
UCLASS()
class SHOOTERGAME_API AShooterGameMode : public AGameMode
{
	GENERATED_BODY()
public:
	

	virtual void PreInitializeComponents() override;

	virtual void PreLogin(const FString& Options, const FString& Address, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage) override;

	virtual void PostLogin(APlayerController* NewPlayer) override;

	virtual AActor* ChoosePlayerStart_Implementation(AController* Player) override;

	virtual bool ShouldSpawnAtStartSpot(AController* Player) override;

	virtual float ModifyDamage(float Damage, AActor* DamagedActor, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) const;

	virtual void Killed(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, const UDamageType* DamageType);

	virtual bool CanDealDamage(AShooterPlayerState* DamageInstigator, AShooterPlayerState* DamagedPlayer) const;

	virtual void DefaultTimer();

	virtual void HandleMatchIsWaitingToStart() override;

	virtual void HandleMatchHasStarted() override;

	virtual void RestartGame() override;



protected:

	AShooterGameMode();

	UPROPERTY(config)
		int32 WarmupTime;

	UPROPERTY(config)
		int32 RoundTime;

	UPROPERTY(config)
		int32 TimeBetweenMatches;

	UPROPERTY(config)
		int32 KillScore;

	UPROPERTY(config)
		int32 DeathScore;

	UPROPERTY(config)
		float DamageSelfScale;

	UPROPERTY(config)
		int32 MaxBots;

	FTimerHandle TimerHandle_DefaultTimer;

	virtual void DetermineMatchWinner();

	virtual bool IsWinner(AShooterPlayerState* PlayerState) const;

	virtual bool IsSpawnpointAllowed(class APlayerStart* SpawnPoint, AController* Player) const;

	virtual bool IsSpawnpointPreferred(class APlayerStart* SpawnPoint, AController* Player) const;


public:

	UFUNCTION(exec)
		void FinishMatch();



	UPROPERTY()
		TArray<class APickUp*> LevelPickups;
	
};
