// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "ShooterGameCharacter.h"
#include "Engine/StaticMeshActor.h"
#include "ShooterGame.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Components/BoxComponent.h"
#include "InventoryComponent.h"
#include "ZoomComponent.h"
#include "ShooterGameInstance.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "ShopBox.h"
#include "GameFramework/InputSettings.h"
#include "ShooterPlayerController.h"
#include "PenetrationComponent.h"
#include "DrawDebugHelpers.h"
#include "UnrealNetwork.h"
#include "PickUp.h"
#include "Gun.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "MotionControllerComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// AShooterGameCharacter

AShooterGameCharacter::AShooterGameCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->RelativeRotation = FRotator(1.9f, -19.19f, 5.2f);
	Mesh1P->RelativeLocation = FVector(-0.5f, -4.4f, -155.7f);

	PickupBox = CreateDefaultSubobject<UBoxComponent>(TEXT("PickUp box"));
	PickupBox->SetupAttachment(RootComponent);


	InventoryComp = CreateDefaultSubobject<UInventoryComponent>(TEXT("InventoryComponent"));
	ZoomComponent = CreateDefaultSubobject<UZoomComponent>(TEXT("ZoomComponent"));


	bIsDead = false;

	Damage = 10;
	CurrentHealth = MaxHealth;
	PickUpReach = 250.f;
	Money = 16000;



	// Note: The ProjectileClass and the skeletal mesh/anim blueprints for Mesh1P, FP_Gun, and VR_Gun 
	// are set in the derived blueprint asset named MyCharacter to avoid direct content references in C++.



}

void AShooterGameCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	if (!HasAuthority())
		PickupBox->Deactivate();

}

void AShooterGameCharacter::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AShooterGameCharacter, AimPitch);
	DOREPLIFETIME(AShooterGameCharacter, CurrentHealth);
	DOREPLIFETIME(AShooterGameCharacter, bIsDead);
}

void AShooterGameCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);



}


void AShooterGameCharacter::SwapUp()
{
	if (InventoryComp->SwapUp())
	{
		ShowInventory();
		EquipPickUp();
		OnSwap.Broadcast();
		UE_LOG(LogTemp, Warning, TEXT("%d"), InventoryComp->GetInventoryIndex());
	}

}

void AShooterGameCharacter::SwapDown()
{

	if (InventoryComp->SwapDown())
	{
		ShowInventory();
		EquipPickUp();
		OnSwap.Broadcast();
		UE_LOG(LogTemp, Warning, TEXT("%d"), InventoryComp->GetInventoryIndex());
	}


}

void AShooterGameCharacter::Server_Reload_Implementation()
{
	Reload();
}

bool AShooterGameCharacter::Server_Reload_Validate()
{
	return true;
}

bool AShooterGameCharacter::IsDead()
{
	return bIsDead;
}

void AShooterGameCharacter::StartCrouch()
{

	Crouch(true);

}

void AShooterGameCharacter::EndCrouch()
{
	UnCrouch();
}



void AShooterGameCharacter::GetPawnMesh(USkeletalMeshComponent*& FirstPerson, USkeletalMeshComponent*& ThirdPerson)
{
	FirstPerson = Mesh1P;
	ThirdPerson = GetMesh();
}



void AShooterGameCharacter::CalculatePawnRotation(float Deltatime)
{
	FRotator DeltaRotator = GetControlRotation() - GetActorRotation();
	FRotator CurrentRotator(AimPitch, 0, 0);
	FRotator Result = FMath::RInterpTo(CurrentRotator, DeltaRotator, Deltatime, 45);
	AimPitch = FMath::ClampAngle(Result.Pitch, -90, 90);
}

FRotator AShooterGameCharacter::GetAimOffsets()
{
	const FVector AimDirection = GetBaseAimRotation().Vector();
	const FVector AimDirLS = ActorToWorld().InverseTransformVectorNoScale(AimDirection);
	const FRotator AimRotationLS = AimDirLS.Rotation();
	return AimRotationLS;

}


void AShooterGameCharacter::Death(float Damage, struct FDamageEvent const & DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	if (bIsDying)return;
	bReplicateMovement = false;
	bTearOff = true;
	bIsDying = true;

	InventoryComp->ClearInventory();
	Mesh1P->SetOwnerNoSee(true);
	DetachFromControllerPendingDestroy();
	PlayDeathAnimation();

}



void AShooterGameCharacter::PlayDeathAnimation_Implementation()
{
	float DeathAnimDuration = 0;
	if (GetMesh() && GetMesh()->AnimScriptInstance)
	{
		GetMesh()->AnimScriptInstance->Montage_Stop(0);
		GetMesh()->SetCollisionProfileName("Ragdoll");
		DeathAnimDuration = GetMesh()->AnimScriptInstance->Montage_Play(DeathAnim);
	}

	if (DeathAnimDuration > 0)
	{
		const float TriggerRagdollTime = DeathAnimDuration - 0.7f;
		GetMesh()->bBlendPhysics = true;
		FTimerHandle DeathTimer;
		GetWorldTimerManager().SetTimer(DeathTimer, this, &AShooterGameCharacter::SetRagdollPhysics, FMath::Max(0.0f, TriggerRagdollTime), false);
	}
	else
	{
		SetRagdollPhysics();
	}
	PickupBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

bool AShooterGameCharacter::PlayDeathAnimation_Validate()
{
	return true;
}

void AShooterGameCharacter::SetRagdollPhysics()
{
	bool bInRagdoll = false;
	if (IsPendingKill())
	{
		bInRagdoll = false;
	}
	else if (!GetMesh() || !GetMesh()->GetPhysicsAsset())
	{
		bInRagdoll = false;
	}
	else
	{
		GetMesh()->SetSimulatePhysics(true);
		GetMesh()->WakeAllRigidBodies();
		GetMesh()->bBlendPhysics = true;
		bInRagdoll = true;
	}

	GetCharacterMovement()->StopMovementImmediately();
	GetCharacterMovement()->DisableMovement();
	GetCharacterMovement()->SetComponentTickEnabled(false);

	if (!bInRagdoll)
	{
		TurnOff();
		SetActorHiddenInGame(true);
		SetLifeSpan(1);
	}
	else
	{
		SetLifeSpan(10);
	}
}

void AShooterGameCharacter::ShowInventory()
{
	AShooterPlayerController* PlayerController = Cast<AShooterPlayerController>(GetController());

	if (!PlayerController)
		return;

	PlayerController->ShowInventory();
}

void AShooterGameCharacter::ThrowPickUp()
{
	if (!InventoryComp->GetCurrentPickup() || !InventoryComp->GetCurrentPickup()->CanThrow())
		return;
	InventoryComp->ThrowPickUp();

}

void AShooterGameCharacter::Reload()
{
	if (Role < ROLE_Authority)
	{
		Server_Reload();
	}
	AGun* CurrentGun = Cast<AGun>(InventoryComp->GetCurrentPickup());
	if (CurrentGun)
	{
		CurrentGun->Reload();
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("no pawn"));
	}
	
	
}

void AShooterGameCharacter::EquipPickUp()
{
	if (!ensure(InventoryComp->GetCurrentPickup()))
		return;

	InventoryComp->GetCurrentPickup()->OnPickedUp(this);

}

void AShooterGameCharacter::OnRep_Health()
{
}

void AShooterGameCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{

	check(PlayerInputComponent);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("PickUp", IE_Pressed, this, &AShooterGameCharacter::CheckForPickUps);

	PlayerInputComponent->BindAction("PauseGame", IE_Pressed, this, &AShooterGameCharacter::PauseGame);

	//InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AShooterGameCharacter::TouchStarted);

	PlayerInputComponent->BindAction("Shop", IE_Pressed, this, &AShooterGameCharacter::AccessShop);
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AShooterGameCharacter::BeginFire);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &AShooterGameCharacter::EndFire);
	PlayerInputComponent->BindAction("RightClick", IE_Pressed, this, &AShooterGameCharacter::ZoomIn);
	PlayerInputComponent->BindAction("RightClick", IE_Released, this, &AShooterGameCharacter::ZoomOut);
	PlayerInputComponent->BindAction("ScrollUp", IE_Pressed, this, &AShooterGameCharacter::SwapUp);
	PlayerInputComponent->BindAction("ScrollDown", IE_Pressed, this, &AShooterGameCharacter::SwapDown);
	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &AShooterGameCharacter::StartCrouch);
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &AShooterGameCharacter::EndCrouch);
	PlayerInputComponent->BindAction("Reload", IE_Pressed, this, &AShooterGameCharacter::Reload);
	PlayerInputComponent->BindAction("ThrowGun", IE_Pressed, this, &AShooterGameCharacter::ThrowPickUp);




	PlayerInputComponent->BindAxis("MoveForward", this, &AShooterGameCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AShooterGameCharacter::MoveRight);

	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AShooterGameCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AShooterGameCharacter::LookUpAtRate);
}



void AShooterGameCharacter::CheckForPickUps()
{
	FVector PlayerLocation;
	FRotator PlayerRotation;
	FVector TraceLine = UpdateRay(300, PlayerLocation, PlayerRotation);
	FHitResult HitResult;
	FCollisionQueryParams CQP;
	CQP.AddIgnoredActor(this);

	GetWorld()->LineTraceSingleByChannel(HitResult, PlayerLocation, TraceLine, ECC_WorldDynamic, CQP);

	APickUp* PickUpActor = Cast<APickUp>(HitResult.GetActor());

	if (!PickUpActor)
	{
		return;
	}

	InventoryComp->EquipPickUp(PickUpActor, true);
	ShowInventory();

}



void AShooterGameCharacter::ResetAim(float DeltaTime)
{

	FRotator PlayerRotation = GetControlRotation();
	FRotator EndRotate(StartFireRotation.Pitch, PlayerRotation.Yaw, PlayerRotation.Roll);
	GetController()->SetControlRotation(FMath::RInterpTo(PlayerRotation, EndRotate, DeltaTime, 3));


}

void AShooterGameCharacter::PauseGame()
{
	UShooterGameInstance* GameInstance = Cast<UShooterGameInstance>(GetGameInstance());
	if (!GameInstance)
		return;

	if (bIsPaused)
	{
		GameInstance->UnPauseGame();
		bIsPaused = !bIsPaused;
		return;
	}
	else
	{
		GameInstance->PauseGame();
		bIsPaused = !bIsPaused;
		return;
	}

}

void AShooterGameCharacter::CanShop(bool Condition)
{
	Shop = Condition;
}



void AShooterGameCharacter::ZoomIn()
{
	ZoomComponent->ZoomIn(true);
}

void AShooterGameCharacter::ZoomOut()
{
	ZoomComponent->ZoomOut(true);
}

void AShooterGameCharacter::ChangeMoney(int32 Amount)
{
	Money += Amount;
}

void AShooterGameCharacter::AccessShop()
{
	AShooterPlayerController* PlayerController = Cast<AShooterPlayerController>(GetController());
	if (!PlayerController || !Shop)
		return;

	if (PlayerController->HUDState == EHUDState::HS_InGame)
	{
		PlayerController->ChangeHUDState(EHUDState::HS_Shop);
	}
	else
	{
		PlayerController->ChangeHUDState(EHUDState::HS_InGame);
	}


}

void AShooterGameCharacter::BeginFire()
{
	if (Role < ROLE_Authority)
	{
		ServerBeginFire();	
	}

	if (InventoryComp->GetCurrentPickup())
	{
		InventoryComp->GetCurrentPickup()->BeginUse();
	}


}

void AShooterGameCharacter::EndFire()
{

	if (Role < ROLE_Authority)
	{
		ServerEndFire();
	}

	if (InventoryComp->GetCurrentPickup())
	{
		InventoryComp->GetCurrentPickup()->EndUse();
	}


}

bool AShooterGameCharacter::ServerBeginFire_Validate()
{
	return true;
}

void AShooterGameCharacter::ServerBeginFire_Implementation()
{
	BeginFire();
}

void AShooterGameCharacter::ServerEndFire_Implementation()
{
	EndFire();
}

bool AShooterGameCharacter::ServerEndFire_Validate()
{
	return true;
}

float AShooterGameCharacter::TakeDamage(float Damage, struct FDamageEvent const & DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	if (Role < ROLE_Authority||bIsDead)return 0.f;
	
	const float ActualDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);

	CurrentHealth -= ActualDamage;
	CurrentHealth = FMath::Clamp(CurrentHealth, 0, MaxHealth);

	GetCharacterMovement()->AddImpulse(GetCharacterMovement()->Velocity.GetSafeNormal()*-5000, true);
	UE_LOG(LogTemp, Warning, TEXT("%d"), CurrentHealth);
	if (CurrentHealth <= 0)
	{
		Death(Damage, DamageEvent, EventInstigator, DamageCauser);
		bIsDead = true;
		DetachFromControllerPendingDestroy();
	}

	return ActualDamage;
}

void AShooterGameCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AShooterGameCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void AShooterGameCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AShooterGameCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

FVector AShooterGameCharacter::UpdateRay(float Range, FVector& OutPlayerLocation, FRotator& OutPlayerRotation)
{
	if (!Controller)return FVector::ZeroVector;
	Controller->GetPlayerViewPoint(OutPlayerLocation, OutPlayerRotation);
	FVector TraceLine = OutPlayerLocation + OutPlayerRotation.Vector()*Range;
	return TraceLine;
}


