// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGameState.h"
#include "UnrealNetwork.h"



void AShooterGameState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AShooterGameState, NumTeams);
	DOREPLIFETIME(AShooterGameState, RemainingTime);
	DOREPLIFETIME(AShooterGameState, bTimerPaused);
	DOREPLIFETIME(AShooterGameState, TeamScores);
}

void AShooterGameState::GetRankedMap(int32 TeamIndex, RankedPlayerMap& OutRankedMap) const
{
	OutRankedMap.Empty();

	TMultiMap<int32, AShooterPlayerState*> SortedMap;

	for (int32 x = 0; x < PlayerArray.Num(); x++)
	{
		int32 Score = 0;
		AShooterPlayerState* CurrentPlayerState = Cast<AShooterPlayerState>(PlayerArray[x]);
		if (CurrentPlayerState&&CurrentPlayerState->GetTeamNum() == TeamIndex)
		{
			SortedMap.Add(FMath::TruncToInt(CurrentPlayerState->Score), CurrentPlayerState);
		}
		
	}

	SortedMap.KeySort(TGreater<int32>());
	
	int32 Rank = 0;

	for(auto& It : SortedMap)
	{
		OutRankedMap.Add(Rank++, It.Value);
	}
}

void AShooterGameState::RequestFinishAndExitToMainMenu()
{

}

