// Fill out your copyright notice in the Description page of Project Settings.

#include "InGameMenu.h"
#include "Components/Button.h"
#include "MenuInterface.h"
#include "Kismet/KismetSystemLibrary.h"



bool UInGameMenu::Initialize()
{
	bool Sucess = Super::Initialize();
	if (!(Sucess))return false;

	if (!ensure(ExitButton) || !ensure(ResumeButton))return false;

	ExitButton->OnClicked.AddDynamic(this, &UInGameMenu::ExitGame);
	ResumeButton->OnClicked.AddDynamic(this, &UInGameMenu::TearDown);

	return true;
}

void UInGameMenu::ExitGame()
{
	if (MenuInterface)
	{
		TearDown();
		MenuInterface->QuitToMenu();
	}
}



