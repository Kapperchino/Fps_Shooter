
#include "BaseMenu.h"
#include "MenuInterface.h"




void UBaseMenu::Setup()
{
	AddToViewport();
	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	if (!ensure(PlayerController))return;

	FInputModeUIOnly InputModeData;
	InputModeData.SetWidgetToFocus(TakeWidget());
	InputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);

	PlayerController->SetInputMode(InputModeData);
	PlayerController->bShowMouseCursor = true;
}

void UBaseMenu::TearDown()
{
	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	if (!ensure(PlayerController))return;
	RemoveFromViewport();
	FInputModeGameOnly GameModeData;
	PlayerController->SetInputMode(GameModeData);
	PlayerController->bShowMouseCursor = false;

}

void UBaseMenu::SetMenuInterface(IMenuInterface* MenuInterface)
{
	this->MenuInterface = MenuInterface;
}

