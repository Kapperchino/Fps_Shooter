// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UI/BaseMenu.h"
#include "InGameMenu.generated.h"

/**
*
*/
UCLASS()
class SHOOTERGAME_API UInGameMenu : public UBaseMenu
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		class UButton* ResumeButton = nullptr;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		class UButton* ExitButton = nullptr;


	UFUNCTION()
		void ExitGame();




protected:

	virtual bool Initialize() override;



};
