// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterPlayerController.h"
#include "ShooterGameCharacter.h"
#include "Blueprint/UserWidget.h"




AShooterPlayerController::AShooterPlayerController()
{
	HUDState = EHUDState::HS_InGame;
}

bool AShooterPlayerController::ApplyHUD(TSubclassOf<class UUserWidget> Widget, bool bShowMouseCursor, bool bEnableClickEvents)
{
	AShooterGameCharacter* MyCharacter = Cast<AShooterGameCharacter>(GetOwner());
	
	if (!Widget)
		return false;

	bShowMouseCursor = bShowMouseCursor;
	bEnableClickEvents = bEnableClickEvents;

	CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), Widget);

	if (!CurrentWidget)
		return false;

	CurrentWidget->AddToViewport();
	return true;
}

void AShooterPlayerController::ApplyHUDChanges()
{
	if (CurrentWidget)
	{
		CurrentWidget->RemoveFromParent();
	}

	switch (HUDState)
	{
	case EHUDState::HS_InGame:
	{
		ApplyHUD(InGameHUD, false, false);
		break;
	}
	case EHUDState::HS_Shop:
	{
		ApplyHUD(ShopHUD, true, true);
		break;
	}
	case EHUDState::HS_Swap:
	{
		ApplyHUD(InventoryHUD, false, false);
		break;
	}
	}
}

void AShooterPlayerController::ChangeHUDState(EHUDState NewState)
{
	HUDState = NewState;
	ApplyHUDChanges();
}

void AShooterPlayerController::ShowInventory()
{
	if (HUDState == EHUDState::HS_InGame)
	{

		GetWorld()->GetTimerManager().SetTimer(Timer, this, &AShooterPlayerController::ResetInventory, 2.f, false);
		ChangeHUDState(EHUDState::HS_Swap);

	}
}

void AShooterPlayerController::ResetInventory()
{
	ChangeHUDState(EHUDState::HS_InGame);
	GetWorld()->GetTimerManager().ClearTimer(Timer);
}

void AShooterPlayerController::OnKill()
{

}

void AShooterPlayerController::BeginPlay()
{

}
