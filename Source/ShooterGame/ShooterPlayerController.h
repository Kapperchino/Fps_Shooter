// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "ShooterPlayerController.generated.h"

UENUM(BlueprintType)
enum class EHUDState : uint8
{
	HS_Shop,
	HS_InGame,
	HS_Swap,

};

UCLASS()
class SHOOTERGAME_API AShooterPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AShooterPlayerController();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EHUDState HUDState;

	bool ApplyHUD(TSubclassOf<class UUserWidget> Widget, bool bShowMouseCursor, bool bEnableClickEvents);

	void ApplyHUDChanges();

	void ChangeHUDState(EHUDState NewState);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		TSubclassOf<class UUserWidget> InGameHUD;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		TSubclassOf<class UUserWidget> ShopHUD;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		TSubclassOf<class UUserWidget> InventoryHUD;

	UPROPERTY()
		class UUserWidget* CurrentWidget = nullptr;

	UFUNCTION(BlueprintCallable)
		void ShowInventory();

	UFUNCTION(BlueprintCallable)
		void ResetInventory();

	void OnKill();



protected:
	void BeginPlay() override;

	FTimerHandle Timer;
	
	
	
};
