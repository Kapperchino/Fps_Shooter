// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "ShooterPlayerState.generated.h"

/**
 * 
 */


UCLASS()
class SHOOTERGAME_API AShooterPlayerState : public APlayerState
{
	GENERATED_BODY()

public:

	virtual void Reset() override;

	virtual void ClientInitialize(class AController* InController) override;

	virtual void UnregisterPlayerWithSession() override;

	void SetTeamNum(int32 NewTeamNum);
	
	void ScoreKill(AShooterPlayerState* Victim, int32 Points);

	void ScoreDeath(AShooterPlayerState* KilledBy, int32 Points);

	FString GetShortPlayerName() const;

	UFUNCTION(Reliable, Client)
	void InformAboutKill(class AShooterPlayerState* KillerPlayerState, const UDamageType* KillerDamageType, class AShooterPlayerState* KilledPlayerState);
	void InformAboutKill_Implementation(class AShooterPlayerState* KillerPlayerState, const UDamageType* KillerDamageType, class AShooterPlayerState* KilledPlayerState);

	UFUNCTION(Reliable, NetMulticast)
	void BroadcastDeath(class AShooterPlayerState* KillerPlayerState, const UDamageType* KillerDamageType, class AShooterPlayerState* KilledPlayerState);
	void BroadcastDeath_Implementation(class AShooterPlayerState* KillerPlayerState, const UDamageType* KillerDamageType, class AShooterPlayerState* KilledPlayerState);


	UFUNCTION()
	void OnRep_TeamColor();

	void SetQuitter(bool bInQuitter);

	virtual void CopyProperties(class APlayerState* PlayerState) override;


	int32 GetTeamNum() const { return TeamNumber; }

	int32 GetKills() const { return NumKills; }

	int32 GetDeaths() const { return NumDeaths; }

	float GetScore() const { return Score; }

	bool IsQuitter() const { return bQuitter; }

protected:

	void UpdateTeamColors();

	UPROPERTY(Transient, ReplicatedUsing = OnRep_TeamColor)
	int32 TeamNumber;

	UPROPERTY(Transient, Replicated)
	int32 NumKills;

	UPROPERTY(Transient, Replicated)
	int32 NumDeaths;

	UPROPERTY()
	bool bQuitter = false;

	void ScorePoints(int32 Points);
	
	AShooterPlayerState();
};
