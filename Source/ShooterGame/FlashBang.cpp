// Fill out your copyright notice in the Description page of Project Settings.

#include "FlashBang.h"
#include "ShooterGameCharacter.h"
#include "Camera/CameraComponent.h"
#include "EngineUtils.h"
#include "DrawDebugHelpers.h"




void AFlashBang::Explode()
{
	UWorld* const World = GetWorld();
	TArray<FHitResult> HitResults;
	FCollisionQueryParams TraceParams;
	TraceParams.bTraceComplex = false;
	TraceParams.AddIgnoredActor(this);
	for (TActorIterator<AShooterGameCharacter> ActorIterator(World); ActorIterator; ++ActorIterator)
	{
		FHitResult HitResult;
		FVector EndTrace;
		FRotator PlayerViewRotation;
		(*ActorIterator)->UpdateRay(0, EndTrace, PlayerViewRotation);
		EndTrace.GetClampedToMaxSize(FlashMaxRange);
		bool bTrace = World->LineTraceSingleByChannel(HitResult, GetActorLocation(), EndTrace, ECC_Visibility, TraceParams);
		DrawDebugLine(World, GetActorLocation(), EndTrace, FColor::Red, false, 3);
		if (bTrace)
		{
			HitResults.Add(HitResult);
		}
	}

	for (int32 x = 0; x < HitResults.Num(); x++)
	{
		if (HitResults[x].GetActor()&&HitResults[x].GetActor()->IsA<AShooterGameCharacter>())
		{
			AShooterGameCharacter* Player = Cast<AShooterGameCharacter>(HitResults[x].GetActor());
			if (Player)
			{
				APlayerController* PlayerController = Cast<APlayerController>(Player->Controller);
				if (PlayerController)
				{
					const float Angle = CalculateFlashAngle(Player, HitResults[x]);
					UE_LOG(LogTemp, Warning, TEXT("%f"), Angle);
				}
			}
		}
	}

	Destroy();
}

float AFlashBang::CalculateFlashAngle(AShooterGameCharacter* const Character, const FHitResult& FlashBangLocation) const
{
	UCameraComponent* const Camera = Character->GetFirstPersonCameraComponent();
	FVector CameraForwardVector = Camera->GetForwardVector();
	FVector CameraLocation = Camera->GetComponentLocation();
	FVector DeltaSafeNormal = (CameraLocation - FlashBangLocation.TraceStart).GetSafeNormal();
	float Result = CameraForwardVector | DeltaSafeNormal;
	float Radiant = FMath::Acos(Result);
	float Angle = 180 - FMath::RadiansToDegrees(Radiant);
	return Angle;
}
