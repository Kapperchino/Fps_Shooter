// Fill out your copyright notice in the Description page of Project Settings.

#include "ZoomComponent.h"
#include "ShooterGameCharacter.h"
#include "Camera/CameraComponent.h"


// Sets default values for this component's properties
UZoomComponent::UZoomComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

void UZoomComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (FMath::IsNearlyEqual(CameraComponent->FieldOfView, GetMaxZoom()))
	{
		bIsZoomedIn = true;
	}
	if (bZoomingIn)
	{
		CameraComponent->FieldOfView = FMath::FInterpTo(CameraComponent->FieldOfView, GetMaxZoom(), DeltaTime, 7.5);
	}
	else
	{
		CameraComponent->FieldOfView = FMath::FInterpTo(CameraComponent->FieldOfView, GetFov(), DeltaTime, 7.5);
	}
	
}


// Called when the game starts
void UZoomComponent::BeginPlay()
{
	Super::BeginPlay();

	AShooterGameCharacter* Player = Cast<AShooterGameCharacter>(GetOwner());
	if (Player)
	{
		CameraComponent = Player->GetFirstPersonCameraComponent();
	}
	
	
}


float UZoomComponent::GetMaxZoom()
{
	return 75;
}

float UZoomComponent::GetFov()
{
	return 100;
}

void UZoomComponent::ZoomIn(bool bHold)
{
	if (!CameraComponent)return;
	
	bZoomingIn = !bZoomingIn;
	

}

void UZoomComponent::ZoomOut(bool bHold)
{
	if (!CameraComponent)return;
	if (bHold)
	{
		bZoomingIn = !bZoomingIn;
	}
	else
	{
		return;
	}
}



