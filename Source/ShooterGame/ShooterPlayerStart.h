// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerStart.h"
#include "ShooterPlayerStart.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API AShooterPlayerStart : public APlayerStart
{
	GENERATED_BODY()

	UPROPERTY(EditInstanceOnly, Category = Team)
	int32 SpawnTeam;


	
	
	
	
};
