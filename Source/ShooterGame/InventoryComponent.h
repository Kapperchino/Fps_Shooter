// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "InventoryComponent.generated.h"

class APickUp;
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SHOOTERGAME_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInventoryComponent();

	const int32 InventorySize = 7;
	

	void ClearInventory();

	bool SwapUp();
	bool SwapDown();

	APickUp* GetSwapDown();
	APickUp* GetSwapUp();


	UFUNCTION(BlueprintCallable)
	UTexture2D* GetThumbnailAtSlot(int32 slot);

	UFUNCTION(BlueprintCallable)
		APickUp* GetCurrentPickup();


	void SetInvntoryIndexToValue(int32 Index, APickUp* Other);

	void PickUp(APickUp* OtherPickup, bool bPickUpByHand);

	UFUNCTION(Server, Unreliable,WithValidation)
	void Server_ThrowPickUp();

	void ThrowPickUp();

	void ClearCurrentPickUp();

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	bool AddItemToInventory(APickUp*& Item, bool bPickUpByHand);

	void EquipPickUp(APickUp* NewPickUp, bool bPickUpByHand);

	int32 GetInventoryIndex();

	int32 GetSpotToAdd(APickUp* Item);

	void ThrowCurrentPickup();

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_EquipPickup(APickUp* NewItem, bool bPickUpByHand);


protected:
	// Called when the game starts
	virtual void BeginPlay() override;


	int32 AmountOfNull();

	UPROPERTY(Transient,Replicated)
	APickUp* CurrentPickUp = nullptr;


	UPROPERTY(Transient,Replicated)
	TArray<APickUp*> Inventory;

	int32 InventoryIndex = 0;

	class AShooterGameCharacter* Player = nullptr;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
	
};
