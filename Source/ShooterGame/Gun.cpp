// Fill out your copyright notice in the Description page of Project Settings.

#include "Gun.h"
#include "UnrealNetwork.h"
#include "ShooterGame.h"
#include "Animation/AnimInstance.h"
#include "ShooterGameCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "Curves/CurveVector.h"


AGun::AGun()
{
	WeaponData = FWeaponData();
	CurrentClipAmmo = WeaponData.ClipAmmo;
	CurrentTotalAmmo = WeaponData.TotalAmmo;
}

void AGun::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(AGun, CurrentClipAmmo, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(AGun, CurrentTotalAmmo, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(AGun, BulletIndex, COND_OwnerOnly);

	DOREPLIFETIME_CONDITION(AGun, bPendingReload, COND_SkipOwner);
}

void AGun::CreateBulletHole(FHitResult& Object)
{
	if (!ensure(WeaponEffects.BulletDecal))
		return;

	FVector BulletHoleSize = FVector(3.5, 7, 7);
	UGameplayStatics::SpawnDecalAtLocation(GetWorld(), WeaponEffects.BulletDecal, BulletHoleSize, Object.ImpactPoint
		, Object.ImpactNormal.Rotation(), 10);
}


void AGun::Reload()
{

	if (!MyPawn)
	{
		UE_LOG(LogTemp, Warning, TEXT("no pawn"));
		return;
	}
	if (WeaponEffects.ReloadAnimation != nullptr)
	{
		UAnimInstance* AnimInstance = MyPawn->GetMesh1P()->GetAnimInstance();
		if (AnimInstance)
		{
			AnimInstance->Montage_Play(WeaponEffects.ReloadAnimation, 1.f);
		}
	}


	int32 AmmoLeft = CurrentTotalAmmo - CurrentClipAmmo;
	int32 AmountToReload = WeaponData.ClipAmmo - CurrentClipAmmo;
	if (CurrentClipAmmo >= WeaponData.ClipAmmo)
		return;
	if (AmountToReload > AmmoLeft)
	{
		CurrentClipAmmo += AmmoLeft;
	}
	else
	{
		CurrentTotalAmmo -= WeaponData.ClipAmmo;
		CurrentClipAmmo = WeaponData.ClipAmmo;
	}

	CurrentClipAmmo = FMath::Clamp(CurrentClipAmmo, 0, WeaponData.ClipAmmo);
	CurrentTotalAmmo = FMath::Clamp(CurrentTotalAmmo, 0, WeaponData.TotalAmmo);
	

}



bool AGun::CanFire()
{
	if (CurrentClipAmmo <= 0)
		return false;
	return true;
}

bool AGun::TraceHit(TArray<FHitResult> &OutHitResult, TArray<FHitResult> &OutReverseHit)
{
	UWorld* const World = GetWorld();
	if (!MyPawn)return false;
	FVector PlayerLocation;
	FRotator PlayerRotation;
	FVector TraceLine = MyPawn->UpdateRay(WeaponData.Range, PlayerLocation, PlayerRotation);

	const FName TraceTag("lol");

	if(bDebugHit)
	World->DebugDrawTraceTag = TraceTag;

	FCollisionQueryParams TraceParams(TraceTag);
	TraceParams.bTraceComplex = false;
	TraceParams.AddIgnoredActor(MyPawn);
	TraceParams.bFindInitialOverlaps = true;
	FCollisionResponseParams ResponseParams(ECR_Overlap);
	FCollisionResponseParams TraceBackResponse(ECR_Block);

	OutHitResult.Init(FHitResult(ForceInitToZero), 10);
	OutReverseHit.Init(FHitResult(ForceInitToZero), 10);
	
	

	bool isTrace = World->LineTraceMultiByChannel(OutHitResult, PlayerLocation, TraceLine, COLLISION_WEAPON, TraceParams, ResponseParams);



	if (OutHitResult.Num() > 0)
	{
		World->LineTraceMultiByChannel(OutReverseHit, OutHitResult[0].TraceEnd, OutHitResult[0].TraceStart, COLLISION_WEAPON, TraceParams, ResponseParams);
	}
	return isTrace;
}

void AGun::ProcessDamage(const TArray<FHitResult>& HitResultList, const TArray<int32>& DamageList)
{
	if (HitResultList.Num() != DamageList.Num())return;
	TSubclassOf<UDamageType> ValidDamgeTypeClass = TSubclassOf<UDamageType>(UDamageType::StaticClass());
	FDamageEvent DamageEvent(ValidDamgeTypeClass);
	for (int32 x = 0; x < HitResultList.Num(); x++)
	{
		CalculateDamageDone(HitResultList[x], DamageList[x]);
		
	}

}

void AGun::CalculateDamageDone(const FHitResult Hit, const int32 Damage)
{
	if (Hit.GetActor()&& Hit.GetActor()->IsA<AShooterGameCharacter>() &&MyPawn&&Damage>0)
	{
		TSubclassOf<UDamageType> ValidDamgeTypeClass = TSubclassOf<UDamageType>(UDamageType::StaticClass());
		FDamageEvent DamageEvent(ValidDamgeTypeClass);

		int32 ActualDamage = Damage;
		AActor* const HitActor = Hit.GetActor();
		if (Hit.BoneName == "head")
		{
			ActualDamage = Damage * 3;
		}
		
		if (WeaponEffects.ImpactBloodParticle)
		{
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponEffects.ImpactBloodParticle, Hit.Location);
		}

		HitActor->TakeDamage(ActualDamage, DamageEvent, MyPawn->Controller, MyPawn);
	}
}

void AGun::MultiCastPlayGunEffects_Implementation()
{
	if (!MyPawn)return;
	if (WeaponEffects.MuzzleParticle)
	{
		UGameplayStatics::SpawnEmitterAttached(WeaponEffects.MuzzleParticle, GetMesh(), WeaponEffects.MuzzleSocketName);
	}
	if (WeaponEffects.FireSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, WeaponEffects.FireSound, GetActorLocation());
	}

	if (WeaponEffects.FireAnimation)
	{
		UAnimInstance* AnimInstance = MyPawn->GetMesh1P()->GetAnimInstance();
		if (AnimInstance)
		{
			AnimInstance->Montage_Play(WeaponEffects.FireAnimation, 1.f);
		}
	}
	HandleRecoil();

}

void AGun::OnFire()
{
	if (!MyPawn || !CanFire())return;

	if (Role < ROLE_Authority)return;

	FMath::Clamp<int32>(CurrentClipAmmo, 0, WeaponData.ClipAmmo);

	CurrentClipAmmo--;
	CurrentTotalAmmo--;
	MultiCastPlayGunEffects();
	

	UWorld* const World = GetWorld();
	TArray<FHitResult> HitResult;
	TArray<FHitResult> ReverseHit;


	bool bTrace = TraceHit(HitResult, ReverseHit);

	FHitResult result;
	FHitResult LastResult;
	Damage = WeaponData.Damage;

	

	float PenetrationLength = 0;

	//UE_LOG(LogTemp, Warning, TEXT("%d"), BulletIndex);

	if (HitResult.Num() > 0)
	{

		CreateBulletHole(HitResult[0]);

		TArray<FHitResult> ActorsHit;
		TArray<int32> DamageList;

		for (int32 x = 0; x < HitResult.Num(); x++)
		{

			result = HitResult[x];

			AActor* Actorhit = result.GetActor();
			AActor*EntryActor = nullptr; AActor* ExitActor = nullptr;
			FHitResult EntryHitResult, ExitHitResult;

			EntryHitResult = HitResult[x];
			ExitHitResult = ReverseHit[HitResult.Num() - 1 - x];
			if (bDebugHit)
			{
				DrawDebugSphere(World, EntryHitResult.Location, 8, 1, FColor::Blue, false, 3);
				DrawDebugSphere(World, ExitHitResult.Location, 8, 1, FColor::Red, false, 3);
			}

			if (LastResult.GetActor() != result.GetActor())
			{
				ActorsHit.Add(result);
				DamageList.Add(Damage);
			}

			PenetrationLength += CalculatePenetrationLength(EntryHitResult, ExitHitResult);


			Damage = CalculatePenetrationDamage(EntryHitResult, Damage, PenetrationLength);
			
			

			LastResult = result;
					
		}

		//UE_LOG(LogTemp, Warning, TEXT("%f"), PenetrationLength);

		ProcessDamage(ActorsHit, DamageList);
	}
	
	BulletIndex++;

	if (WeaponData.bAuto)
		GetWorldTimerManager().SetTimer(GunTimer, this, &AGun::OnReFire, WeaponData.FireRate, false);
}

void AGun::BeginUse()
{
	OnFire();

	BulletIndex = 0;
	bWantsToFire = true;
}

void AGun::EndUse()
{
	bWantsToFire = false;
}

void AGun::OnReFire()
{
	if (bWantsToFire)
	{
		OnFire();
	}
}

void AGun::AddAmmo(int32 Amount)
{
	int32 MaxAmount = WeaponData.TotalAmmo - WeaponData.ClipAmmo;
	int32 AmmoLeft = CurrentTotalAmmo - CurrentClipAmmo;
	AmmoLeft += Amount;
	FMath::Clamp(AmmoLeft, 0, MaxAmount);
	CurrentTotalAmmo = AmmoLeft + CurrentClipAmmo;
}

float AGun::CalculatePenetrationLength(FHitResult& EntryHit, FHitResult& ExitHit)
{
	AActor* EntryActor = EntryHit.GetActor();
	AActor*ExitActor = ExitHit.GetActor();
	float PenetrationLength = 0;
	if (EntryActor&&ExitActor&&ExitActor == EntryActor)
	{
		PenetrationLength = (ExitHit.Location - EntryHit.Location).Size();
	}

	return PenetrationLength;
}

float AGun::CalculatePenetrationDamage(FHitResult& EntryHit, float Damage, float Distance)
{
	EPhysicalSurface Surface = UGameplayStatics::GetSurfaceType(EntryHit);
	float PenetrationConstant = 0;

	switch (Surface)
	{
	case SurfaceType_Default:
		PenetrationConstant = 0.1;
		break;
	case SurfaceType1:
		PenetrationConstant = 0.1;
		break;
	default:
		break;
	}
	float ActualDamage = Damage - (PenetrationConstant*Distance);
	if (ActualDamage < 0)
		return 0;
	return ActualDamage;
}

void AGun::HandleRecoil()
{
	if (!WeaponData.Recoil || !MyPawn)return;
	MyPawn->AddControllerPitchInput(WeaponData.Recoil->GetVectorValue(BulletIndex).Y);
	MyPawn->AddControllerYawInput(WeaponData.Recoil->GetVectorValue(BulletIndex).X);
}


